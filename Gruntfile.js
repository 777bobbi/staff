module.exports = function(grunt) {
    var pkgConfig = grunt.file.readJSON('package.json');

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Project configuration.
    grunt.initConfig({
        pkg: pkgConfig,
        meta: {
            banner: ['/*',
                ' * Project name: <%= pkg.name %>',
                ' * Version: <%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>',
                ' */\n'].join('\n'),
            isolateModule: {
                banner: '!function(angular) {\n',
                footer: '\n} (window.angular);'
            }
        },

        clean: {
            dist: ["dist"],
            tmp: ['.tmp']
        },

        html2js: {
            allTemplates: {
                options: {
                    module: pkgConfig.name + '.tpls'
                },
                src: ['src/*/templates/**/*.html'],
                dest: '.tmp/templates/all-tpls.js'
            }
        },

        concat: {
            vendor_js: {
                src: [
                    'vendor/jquery/dist/jquery.js',
                    'vendor/angular/angular.js',
                    'vendor/angular-cookies/angular-cookies.js',
                    'vendor/angular-ui-router/release/angular-ui-router.js',
                    'vendor/ng-grid/build/ng-grid.js',
                    'vendor/angular-local-storage/angular-local-storage.js',
                ],
                dest: '.tmp/scripts/vendor.js'
            },
            staff_js: {
//                options: {
//                    banner: '<%= meta.isolateModule.banner %>',
//                    footer: '<%= meta.isolateModule.footer %>'
//                },
                src: ['src/staff/scripts/**/*.js'],
                dest: '.tmp/scripts/staff.js'
            },
            auth_js: {
                src: ['src/auth/scripts/**/*.js'],
                dest: '.tmp/scripts/auth.js'
            },
            data_js: {
                src: ['src/data/scripts/**/*.js'],
                dest: '.tmp/scripts/data.js'
            },
            translate_js: {
                src: ['src/translate/scripts/**/*.js'],
                dest: '.tmp/scripts/translate.js'
            },

            vendor_css: {
                src: ['vendor/bootstrap/dist/css/bootstrap.css'],
                dest: '.tmp/styles/vendor.css'
            },
            staff_css: {
                src: [
                    'src/staff/styles/reset.css',
                    'src/staff/styles/login-page.css',
                    'src/staff/styles/styles.css'
                ],
                dest: '.tmp/styles/staff.css'
            },

            dist: {
                files: {
                    'dist/scripts/vendor.js': ['<%= concat.vendor_js.dest %>'],
                    'dist/scripts/app.js': [
                        '<%= concat.staff_js.dest %>',
                        '<%= concat.auth_js.dest %>',
                        '<%= concat.data_js.dest %>',
                        '<%= concat.translate_js.dest %>',
                        '<%= html2js.allTemplates.dest %>'
                    ],

                    'dist/styles/vendor.css': ['<%= concat.vendor_css.dest %>'],
                    'dist/styles/app.css': ['<%= concat.staff_css.dest %>']
                }
            }
        },

//        uglify: {
//            options: {
//                banner: '<%= meta.banner %>'
//            },
//            staff:{
//                src:['<%= concat.staff.dest %>'],
//                dest:'dist/scripts/staff.min.js'
//            }
//        },
//        cssmin: {
//            staff: {
//                files: {
//                    'dist/styles/staff.min.css': ['src/staff/styles/*.css']
//                }
//            }
//        },
//
//        jshint: {
//            options: {
//                jshintrc: '.jshintrc',
//                reporter: require('jshint-stylish')
//            },
//            all: [
//                'Gruntfile.js',
//                'src/**/*.js'
//            ]
//        },

        copy: {
            staff: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/staff/images/',
                        src: '**',
                        dest: 'dist/images/',
                        filter: 'isFile'
                    }
                ]
            }
        },

        watch: {
            staff: {
                files: [
                    'Gruntfile.js',
                    'src/**/*.js',
                    'src/**/*.css',
                    'src/**/*.html'
                ],
                tasks: ['default']
            }
        }
    });


    grunt.registerTask('build', [
        'clean',
//        'jshint',
        'html2js',
        'concat',
        'copy',
        'clean:tmp'
    ]);

    grunt.registerTask('default', ['build','watch']);

};