/**
 * Partner data module.
 *
 * Provides access to data on server side.
 */
angular.module('tgStuff.data', [])

    .config(['$httpProvider', function ($httpProvider) {

        $httpProvider.defaults.headers.common = {'X-Requested-With': 'XMLHttpRequest'};
        $httpProvider.interceptors.push('TransHttpInterceptor');

    }])

    .factory({
        TransHttpInterceptor: TransHttpInterceptor,
        TigerResource: TigerResourceFactory,

        // Entity's repositories
        CurrentUserData: CurrentUserData,

    })

    .service({

    })
;