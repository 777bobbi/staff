TransHttpInterceptor.$inject = ['$q'];
function TransHttpInterceptor($q) {

    return {
        'response': function (response) {
            return response || $q.when(response);
        },

        'responseError': function (rejection) {
            return $q.reject(rejection);
        }
    };

}