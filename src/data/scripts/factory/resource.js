TigerResourceFactory.$inject = ['$http'];
function TigerResourceFactory($http) {

    var BASE_URL = buildBaseUrl();

    return function ResourceFactory() {
        var cfg;

        if (angular.isObject(arguments[0])) {
            cfg = arguments[0];
        }
        else {
            cfg = {
                method: 'GET',
                url: arguments[0]
            };
        }

        cfg.url = BASE_URL + cfg.url;


        return function () {
            var data, success, error;

            data = angular.isFunction(arguments[0]) ? null : arguments[0];
            success = angular.isFunction(arguments[0]) ? arguments[0] : arguments[1];
            error = angular.isFunction(arguments[0]) ? arguments[1] : arguments[2];


            var param = (cfg.method == 'GET' ? 'params' : 'data');
            cfg[param] = data;

            $http(cfg)
                .then(function (response) {
                    (success || angular.noop)(response.data, response.status, response.headers, response.config);
                    return response.data;
                }, function (response) {
                    (error || angular.noop)(undefined, response.status, response.headers, response.config);
                    return undefined;
                });
        };
    };

    /**
     * @returns {string}
     */
    function buildBaseUrl() {
        var str = new String(window.location);

        if (str.search(/app_dev/) != -1) {
            return '/app_dev.php/api';
        }

        return '/api';
    }
}