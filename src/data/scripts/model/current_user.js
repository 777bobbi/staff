CurrentUserData.$inject = ['TigerResource'];
function CurrentUserData(TigerResource) {

    return {
        whoami: TigerResource('/me'),
        login: TigerResource({url: '/me/login', method: 'POST'}),
        logout: TigerResource('/me/logout'),
        restorePassword: TigerResource('/me/password/restore'),
        changePassword: TigerResource({url: '/me/password/change', method: 'POST'}),
        getSponsor: TigerResource('/me/sponsor')
    };

}