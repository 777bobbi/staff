UsersData.$inject = ['TigerResource'];
function UsersData(TigerResource) {

    return {
        getTeam: TigerResource('/my/team'),
        getPartnersCount: TigerResource('/user/statistics/partnerscount'),
        getVisitsCount: TigerResource('/user/statistics/visitscount'),
        getStructureDepth: TigerResource('/user/statistics/structuredepth'),
        getStructureSize: TigerResource('/user/statistics/structuresize'),
        getDaysInSystem: TigerResource('/user/statistics/daysinsystems'),
        getLoginCount: TigerResource('/user/statistics/logincount'),
        getConversion: TigerResource('/user/statistics/conversion'),
        getLeaders: TigerResource('/user/leaders'),
        getInfo: TigerResource('/user/info'),
        getVisitsStatistics: TigerResource('/user/statistics/visits'),
        getPaymentHistory: TigerResource('/me/paymenthistory'),
        getPayee: TigerResource('/user/payee'),
        getChannels: TigerResource('/me/channels'),
        transfer: TigerResource({url: '/user/transfer', method: 'POST'}),
        addChannel: TigerResource({url: '/me/addchannel', method: 'POST'}),
        getLastRegistrations: TigerResource('/user/lastregistrations'),
        contactExport: TigerResource('/my/team/export')
    };

}