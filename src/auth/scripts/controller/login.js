LoginController.$inject = ['$auth', '$state', '$scope', 'localStorageService'];
function LoginController($auth, $state, $scope, localStorageService) {

    /**
     * Determines that checking credentials are in progress.
     *
     * @type {boolean}
     */
    $scope.checking = false;
    $scope.remember = !$.isEmptyObject(localStorageService.get('remember'));

    /**
     * Submit callback.
     */
    $scope.login = function () {
        $scope.checking = true;
        $scope.LoginMessage = null;

        $auth.login({
            email: $scope.email,
            password: $scope.password,
            remember: $scope.remember
        }, loginSuccessfully, loginFailure);
    };


    function loginSuccessfully() {
//        localStorageService.clearAll();

        logins = localStorageService.get('logins');

        if ($scope.email && $scope.password && $scope.remember) {
            localStorageService.add('remember', JSON.stringify({email: $scope.email, password: $scope.password}));
        } else {
            localStorageService.remove('remember');
        }

        if (logins == null) {
            logins = [arguments[0].email];
            localStorageService.add('logins', JSON.stringify(logins));
        }
        else {
            logins = localStorageService.get('logins');

            if (logins.indexOf(arguments[0].email) == -1) {
                logins[logins.length] = arguments[0].email;
                localStorageService.add('logins', JSON.stringify(logins));
            }
        }

        $scope.checking = false;
        $state.go('auth.load');
    }

    function loginFailure(msg) {
        $scope.checking = false;
        $scope.LoginMessage = msg || $translate.translate('GENERAL_ERRORS_UNKNOWN_ERROR');
    }

    $scope.getLogins = function () {
        $scope.logins = localStorageService.get('logins');
        $scope.autocomplete = true;
    }

    $scope.selectLogin = function ($event) {
        $scope.autocomplete = false;
        $scope.email = $event.target.innerHTML;
    }
}