LogoutController.$inject = ['$auth', '$state','TaskQueueService', 'localStorageService'];
function LogoutController($auth, $state, TaskQueueService, localStorageService) {

    $auth.logout(function () {
        if (!$.isEmptyObject(localStorageService.get('remember'))) {
            localStorageService.remove('remember');
        }

        $state.go('auth.login');
        TaskQueueService.destroy();
    });

}