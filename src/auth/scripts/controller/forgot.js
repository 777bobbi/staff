ForgotPasswordController.$inject = ['$scope', '$state', '$timeout', 'CurrentUserData', '$translate'];
function ForgotPasswordController($scope, $state, $timeout, CurrentUserData, $translate) {

    /**
     * Determines that restoring password is in progress.
     *
     * @type {boolean}
     */
    $scope.restoring = false;


    /**
     * Submit callback.
     */
    $scope.restore = function () {
        $scope.restoring = true;
        $scope.RestoreMessage = null;

        CurrentUserData.restorePassword({
            email: $scope.email
        }, restorePasswordSuccessfully, restorePasswordFailure);
    };


    function restorePasswordSuccessfully(data) {
        $scope.restoring = false;

        if (statusCode['Ok'] == data.code) {
            $scope.restored = true;

            $timeout(function () {
                $state.go('auth.login');
            }, 3000);
        }
        else if (statusCode['AUTH_USER_NOT_EXISTS'] == data.code) {
            $scope.RestoreMessage = $translate.translate('AUTH_UNKNOWN_USER');
        }
        else {
            $scope.RestoreMessage = $translate.translate('GENERAL_ERRORS_UNKNOWN_ERROR');
        }
    }

    function restorePasswordFailure(msg) {
        $scope.restoring = false;
        $scope.RestoreMessage = msg || $translate.translate('GENERAL_ERRORS_UNKNOWN_ERROR');
    }

}