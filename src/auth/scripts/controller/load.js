LoadController.$inject = ['$scope', '$state', '$translate'];
function LoadController($scope, $state, $translate) {

    $scope.msg = $translate.translate('AUTH_LOADING_CONTENT');

    $state.go('app.clients');

}