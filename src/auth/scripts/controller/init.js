InitController.$inject = ['$rootScope', '$scope', '$auth', '$state', '$translate'];
function InitController($rootScope, $scope, $auth, $state, $translate) {

    function go(name) {
        return function () {
            $state.go(name);
        }
    }

    $scope.msg = $translate.translate('AUTH_LOADING_SYSTEN_INFO');
    $auth.authorize(go('auth.load'), go('auth.login'));

}