/**
 * Builds a distinct bit mask for each role.
 *
 * @param roles
 * @returns {Object}
 */
function buildMap(roles) {
    var bitMask = '01';
    var masks = {};

    for (var role in roles) {
        var intCode = parseInt(bitMask, 2);

        masks[roles[role]] = intCode;
        bitMask = (intCode << 1).toString(2)
    }

    return masks;
}


/**
 * Builds access level bit masks.
 *
 * @param acl
 * @param roles
 * @returns {Object}
 */
function buildAccessLevels(acl, roles) {
    var masks = {};

    for (var level in acl) {
        var bitMask = 0;

        for (var role in acl[level]) {
            if (roles.hasOwnProperty(acl[level][role])) {
                bitMask = bitMask | roles[acl[level][role]];
            }
        }

        masks[level] = bitMask;
    }

    return masks;
}


/**
 * Access config.
 *
 * @type {Object}
 */
var accessConfig = {
    roles: [
        'public',
        'anon',
        'partner',
        'owner'
    ],
    levels: {
        'public': ['public', 'anon', 'partner', 'owner'],
        'anon': ['anon'],
        'partner': ['partner', 'owner'],
        'owner': ['owner']
    }
};

var roles = buildMap(accessConfig.roles);
var acl = buildAccessLevels(accessConfig.levels, roles);