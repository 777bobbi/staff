AuthFactory.$inject = ['$rootScope', '$cookieStore', 'CurrentUserData', '$translate', 'localStorageService'];
function AuthFactory($rootScope, $cookieStore, CurrentUserData, $translate, localStorageService) {

    var currentUser = {
        role: roles.public,

        // todo: take these parameters away from here
        protocol: location.protocol,
        host: location.host
    };

    $rootScope.me = currentUser;

    return {
        granted: function (accessLevel) {
            return accessLevel & currentUser.role;
        },
        isAnonymous: function () {
            return roles.anon & currentUser.role;
        },
        isAuthorized: function () {
            return roles.partner & currentUser.role;
        },
        authorize: function (success, error) {

            CurrentUserData.whoami(function (env) {
                angular.extend(currentUser, env.data, {
                    role: roles.partner
                });

                success(currentUser);
            }, function () {
                var remember = localStorageService.get('remember');

                if (!$.isEmptyObject(remember) && remember.email && remember.password) {
                    CurrentUserData.login(remember, function (env) {
                        if (statusCode['Ok'] == env.code) {
                            angular.extend(currentUser, env.data, {
                                role: roles.partner
                            });

                            success(currentUser);
                        } else {
                            currentUser.role = roles.anon;
                            error();
                        }
                    }, function () {
                        currentUser.role = roles.anon;
                        error();
                    });

                    return;
                }

                currentUser.role = roles.anon;
                error();
            });

        },
        login: function (user, success, error) {

            CurrentUserData.login(user, function (env) {
                if (statusCode['Ok'] == env.code) {
                    angular.extend(currentUser, env.data, {
                        role: roles.partner
                    });

                    success(currentUser);
                } else if (statusCode['AUTH_USER_IS_LOCKED'] == env.code) {
                    error($translate.translate('AUTH_ACCOUNT_BLOCKED'));
                } else {
                    error($translate.translate('AUTH_WRONG_USERNAME_OR_PASSWORD'));
                }
            }, function () {
                currentUser.role = roles.anon;
                error();
            });

        },
        logout: function (success) {
            $cookieStore.remove('PHPSESSID');
            currentUser.role = roles.anon;

            CurrentUserData.logout();
            success();
        }
    };

}