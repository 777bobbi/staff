/**
 * Partner access module.
 */
angular.module('tgStuff.access', [
        'ngCookies',
        'tgStuff.data',
        'LocalStorageModule'
    ])

    .config(['$stateProvider', function tgAccessConfig($stateProvider) {

        $stateProvider
            .state('auth', {
                abstract: true,
                url: '',
                templateUrl: 'auth/templates/main.html'
            })
            .state('auth.init', {
                url: '',
                controller: 'InitCtrl',
                templateUrl: 'auth/templates/init.html"',
                access: acl.public
            })
            .state('auth.login', {
                url: '/login',
                controller: 'LoginCtrl',
                templateUrl: 'auth/templates/login.html',
                access: acl.anon
            })
            .state('auth.forgot', {
                url: '/forgot',
                controller: 'ForgotPasswordCtrl',
                templateUrl: 'auth/templates/forgot.html',
                access: acl.anon
            })
            .state('auth.load', {
                url: '',
                controller: 'LoadCtrl',
                templateUrl: 'auth/templates/load.html',
                access: acl.partner
            })
            .state('auth.logout', {
                url: '/logout',
                controller: 'LogoutCtrl',
                templateUrl: 'auth/templates/init.html',
                access: acl.partner
            })
        ;

    }])

    .controller({
        InitCtrl: InitController,
        LoadCtrl: LoadController,
        LoginCtrl: LoginController,
        LogoutCtrl: LogoutController,
//        ForgotPasswordCtrl: ForgotPasswordController
    })

    .factory({
        $auth: AuthFactory
    })
;