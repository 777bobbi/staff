//now ie8 understand tag translate
document.createElement('TRANSLATE');
angular.module('tgTranslate',[])
    .factory('translateFile', ['$q', '$http', function($q, $http){
        var translate = '';
        $.ajax(
            {
                url:'../api/system/locale/partner',
                async: false,
                dataType: "json"
            })
            .then(function(data){
                translate = data.data;
            }
        );
        var currentLag = '';
        $.ajax(
            {
                url:'../api/system/currentLocale/partner',
                async: false,
                dataType: "json"
            })
            .then(function(data){
                currentLag = data.data;
            }
        );

        var staticTranslation = {};

       $.ajax(
            {
                url:'/bundles/partner/locale/' + currentLag + '.json',
                async: false,
                dataType: "json"
            })
            .then(function(data){
                staticTranslation = data;
            }
        );

        return angular.extend(translate, staticTranslation);

    }])
    .provider('$translate', function(){
        return {
            $get:function(){
                return this;
            },
            translate: function(key, data){
                var trans = key.split('.');
                var wrd = this.localeData || {};

                for (var i = 0; i < trans.length; i++) {
                    if( !wrd[trans[i]]) {
                        return  key;
                    }
                    wrd = this.replacePlaceHolders(wrd[trans[i]], data);
                }

                return wrd;
            },
            replacePlaceHolders: function(string, placeHolders) {
                if (!placeHolders) {
                    return string;
                }
                for (i in placeHolders) {
                    var regExp = new RegExp('%' + i + '%','g');
                    if (string != undefined){
                        string = string.replace(regExp, placeHolders[i]);
                    }
                }
                return string;
            }
        }
    })
    .filter('translate', ['$rootScope', '$translate', function($rootScope, $translate){
        return function(langKey, data){
            return $translate.translate(langKey, data);

        }
    }])
    .directive('translate', ['$http', '$translate', '$rootScope', function($http, $translate, $rootScope){
        return {
            restrict: 'AE',
            scope: true,
            link: function linkFn(scope, element, attr) {
                scope.attr = {};
                for (i in attr) {
                    if(0 == i.indexOf('$')) {
                        continue;
                    }
                    scope.attr[i] = attr[i];
                }

                for (i in scope.attr) {
                    if(0 == i.indexOf('$')) {
                        continue;
                    }
                    (function() {
                        var name = i.replace(/([A-Z])/g,function(char){
                            return '-' + char.toLowerCase()
                        });
                        scope.$watch(function(){
                            return element.attr(name)
                        },function(value){
                            var placeHolder = name.replace(/(-[a-z])/g,function(char){
                                return char.replace('-','').toUpperCase()
                            });
                            scope.attr[placeHolder] = value;
                            var translateVal = $translate.translate( element.attr('origin-translate'), scope.attr);
                            element.html(translateVal);
                        })
                    })();
                }
                element.attr('origin-translate', element.text());
                var translateVal = $translate.translate(element.text(), scope.attr);

                element.html(translateVal);
            }
        };
    }])
    .run(['$translate', 'translateFile', function( $translate,translateFile){

        $translate.localeData = translateFile;
        window.translate = $translate;

    }])