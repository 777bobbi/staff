DateDiff.$inject = ['$translate'];

function DateDiff() {

    var secondInMs = 1000;
    var minutesInMs = secondInMs * 60;
    var hoursInMs = minutesInMs * 60;
    var daysInMs = hoursInMs * 24;

    return function (time, currentTime) {
        if (!time) {
            return '';
        }

        var time = Date.parse(time);
        var now = new Date();
        var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
        var currentTime = currentTime ? Date.parse(currentTime) : now_utc;
        var timeDiff = Math.abs(currentTime - time);

        var daysDiff = Math.floor((timeDiff) / daysInMs);
        var hoursDiff = Math.floor((timeDiff - (daysDiff * daysInMs)) / hoursInMs);
        var minutesDiff = Math.floor((timeDiff - ((daysDiff * daysInMs) + (hoursDiff * hoursInMs))) / minutesInMs);

        var returnString = '';
        if (daysDiff) {
            returnString += daysDiff + window.translate.translate('GENERAL_DAYS');
        }
        if (hoursDiff) {
            returnString += hoursDiff + window.translate.translate('GENERAL_HOURS');
        }
        if (minutesDiff) {
            returnString += minutesDiff + window.translate.translate('GENERAL_MONTHES');
        }

        return returnString;
    };
}