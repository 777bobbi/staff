Replace.$inject = [];

function Replace() {
    return function (text, pattern, replacement) {
        return text.replace(pattern, replacement);
    };
}