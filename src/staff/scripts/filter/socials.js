InArray.$inject = [];

function InArray() {

    return function (haystack, field, needles) {
        var ret = [];

        angular.forEach(haystack, function(v){
            if(angular.isDefined(v[field]) && needles.indexOf(v[field]) != -1){
                ret.push(v);
            }
        });

        return ret;
    };
}