TaskQueueService.$inject = ['$interval'];
function TaskQueueService($interval) {
    this.$interval = $interval;
    this.$queue = [];
}

TaskQueueService.prototype = {
    frequency: 1000,

    intervalFunction: undefined,

    /**
     * Registers a task with its parameters.
     *
     * @param fn
     * @param interval - Number of milliseconds between each task call.
     * @returns {Task}
     */
    registerTask: function (fn, interval, name) {
        var t = new Task(fn, interval, name);
        this.$queue.push(t);

        if (this.$queue.length == 1) {
            this.run();
        }

        return t;
    },

    unRegisterTask: function (name) {
        var me = this;

        angular.forEach(me.$queue, function (task, index) {
            if (name == task.name) {
                me.$queue.splice(index, 1);

                if (me.$queue.length == 0) {
                    me.stop();
                }

                return true;
            }
        });

        return false;
    },

    setTaskIsCompleted: function (name, status) {
        var me = this;
        status = status || false;

        angular.forEach(me.$queue, function (task, index) {
            if (name == task.name) {
                task.isRunning = status;

                return true;
            }
        });

        return false;
    },

    changeFrequency: function (value) {
        value = parseInt(value);
        if (0 < value) {
            this.frequency = value;

            return this.frequency;
        }

        return false;
    },

    setTaskInterval: function (name, interval) {
        var me = this;
        interval = parseInt(interval);

        if (0 < interval) {
            angular.forEach(me.$queue, function (task, index) {
                if (name == task.name) {
                    task.interval = interval;

                    return task.interval;
                }
            });
        }

        return false;
    },

    /**
     * Runs a loop.
     */
    run: function () {
        var me = this;

        this.intervalFunction = this.$interval(function () {
            var now = Date.now();

            angular.forEach(me.$queue, function (task) {
                if (now >= task.nextExecution && false == task.isRunning) {
                    task.run();
                }
            });

        }, me.frequency);
    },

    stop: function () {
        this.$interval.cancel(this.intervalFunction);
    },

    destroy: function() {
        this.stop();
        this.$queue = [];
    }
};


/**
 * This function encapsulates some external task with time,
 * which specify a execution time, to run.
 *
 * @param fn
 * @param interval
 * @constructor
 */
function Task(fn, interval, name) {
    this.fn       = fn;
    this.interval = interval;
    this.name     = name;

    this.fn();
    this.calcNextExecutionTime();
}

Task.prototype = {
    isRunning: true,

    /**
     * Calculates next execution time parameter.
     */
    calcNextExecutionTime: function () {
        var now = Date.now();
        now += this.interval;
        this.nextExecution = now;
    },

    /**
     * Runs special task.
     */
    run: function () {
        this.isRunning = true;
        this.fn();
        this.calcNextExecutionTime();
    }
};