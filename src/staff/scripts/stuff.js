/**
 * Stuff module.
 */

angular.module('tgStuff', [
        'ngGrid',
        'ui.router',
        'TgStaffCab.tpls',
        'tgStuff.data',
        'tgStuff.access',
        'tgTranslate',
        'LocalStorageModule'
    ])

    .config(['$stateProvider', '$urlRouterProvider', '$translateProvider', '$compileProvider', function tgStuffConfig($stateProvider, $urlRouterProvider, $translate, $compileProvider) {

        $compileProvider
            .aHrefSanitizationWhitelist(/^\s*(https?|mailto|skype):/);


        $urlRouterProvider
            .otherwise('/404');


        $stateProvider
            .state('app', {
                abstract: true,
                controller: 'AppCtrl',
                templateUrl: 'staff/templates/main.html',
                access: acl.partner
            })
            .state('app.clients', {
                url: '/clients',
                views: {
                    'content': {
                        controller: 'ClientsCtrl',
                        templateUrl: 'staff/templates/clients.html'
                    }
                },
                access: acl.partner
            })
            .state('app.balance', {
                url: '/balance',
                views: {
                    'content': {
                        controller: 'BalanceCtrl',
                        templateUrl: 'staff/templates/balance.html'
                    }
                },
                access: acl.partner
            });
    }])

    .run(['$rootScope', '$state', '$auth', '$filter', function tgAccessRun($rootScope, $state, $auth, $filter) {

        function storeRequestedState(state, params) {
            if (state.access & acl.anon) {
                return;
            }

            $rootScope.requestedRoute = {
                state: state.name,
                params: params
            };
        }

        $rootScope.$filter = $filter;

    }])

    .controller({
        AppCtrl: AppController,
        ClientsCtrl: ClientsController,
        BalanceCtrl: BalanceController
    })

    .factory({

    })

    .directive({

    })

    .service({
        TaskQueueService: TaskQueueService
    })

    .filter({
        //dateDiff: DateDiff,
        //inArray: InArray,
        //replace: Replace
    })
;