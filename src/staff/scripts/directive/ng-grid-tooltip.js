NgGridToolTip.$inject = [];

function NgGridToolTip(){
    return {
        restrict: 'A',
        replace: true,
        link: function(scope, element, attrs){

            $('body').append('<div id="tooltip" class="tooltip bottom fade in" title="" style="display: none; position: absolute; top: 0; left: 0">'+
                                '<div class="tooltip-arrow"></div>'+
                                '<div class="tooltip-inner ng-binding" style="min-width: 100px; max-width: 300px"></div>'+
                             '</div>');

            for (var i = 0; i < element.length; i++){
                element[i].onmouseover = function(e){

                    if ($('#tooltip').css('display') == 'none')
                    {
                        $('#tooltip .tooltip-inner').text(attrs.tgNgGridTooltip);
                        $('#tooltip').css('top', e.pageY);
                        $('#tooltip').css('left', e.pageX - 90);

                        $('#tooltip').show();
                    }
                };

                element[i].onmouseout = function(){
                    $('#tooltip').hide();
                    $('#tooltip .tooltip-inner').text('');
                };
            }

        }
    }
}