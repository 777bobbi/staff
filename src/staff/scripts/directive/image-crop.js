function ImageCropDirective() {

    return {
        restrict: 'A',
        template: '',
        link: function (scope, element, attrs) {
            var element = $(element),
                options = attrs['tgImgCropConfig'] ? JSON.parse(attrs['tgImgCropConfig'].replace(/'/g, '"')) : {},
                model = attrs['tgImgCropModel'],
                coords = {},
                zoomWidthCoefficient = false,
                zoomHeightCoefficient = false;

            if (model) {
                if (!scope[model]) {
                    scope[model] = {};
                }

                coords = scope[model];
            }

            options.onChange = options.onSelect = setCoords;

            element.Jcrop(options);


            function setCoords(selection) {
                setZoomCoefficient();
                coords.x1 = (selection.x >= 0 ? selection.x : 0) * zoomWidthCoefficient;
                coords.y1 = (selection.y >= 0 ? selection.y : 0) * zoomHeightCoefficient;
                coords.x2 = (selection.x2 >= 0 ? selection.x2 : 0) * zoomWidthCoefficient;
                coords.y2 = (selection.y2 >= 0 ? selection.y2 : 0) * zoomHeightCoefficient;
            }

            function setZoomCoefficient() {
                if (zoomWidthCoefficient && zoomHeightCoefficient) {
                    return;
                }

                var oldWidth = element.width();
                var oldHeight = element.height();

                element.height('auto').width('auto');

                var width = element.width();
                var height = element.height();

                zoomWidthCoefficient = width / oldWidth;
                zoomHeightCoefficient = height / oldHeight;

                element.height(zoomHeightCoefficient).width(zoomWidthCoefficient);
            }
        }
    };

}