function FixAutofill() {

    genElements = function (appendElement, desc) {
        if (desc.closeTag)
            element = $('<' + desc.elementName + '></' + desc.elementName + '>');
        else
            element = $('<' + desc.elementName + '>');

        for (prop in desc.attr) {
            if (prop == 'elementName' || prop == 'closeTag')
                continue;
            element.attr(prop, desc.attr[prop]);
        }


        appendElement.append(element);


        if (desc.elements != undefined && desc.elements.length > 0) {
            for (var i = 0; i < desc.elements.length; i++)
                genElements(element, desc.elements[i])
        }
    }

    return {
        restrict: "A",
        scope: {
            onSubmit: "=tgFixAutofill"
        },
        link: function (scope, element, attrs) {
            $(element)[0].onsubmit = function () {

                var hideForm = {
                    elementName: 'form',
                    closeTag: true,
                    attr: {id: "login-form", method: "post", target: "fixAutofillFrame"},
                    elements: [
                        {
                            elementName: 'input',
                            closeTag: false,
                            attr: {
                                id: 'login-email',
                                name: 'email',
                                type: 'text'
                            }
                        }
                    ]
                }

                var hideFrame = {
                    elementName: 'iframe',
                    closeTag: true,
                    attr: {
                        id: "fixAutofillFrame",
                        name: "fixAutofillFrame",
                        width: 0,
                        height: 0
                    }
                }
                //create form and iframe
                var form = genElements($('body'), hideForm);
                var frame = genElements($('body'), hideFrame);


                $("body").append(form);
                $("body").append(frame);

                $("#login-email").val($("[name=email]", element).val());
                $("#login-password").val($("[name=password]", element).val());

                $("#login-form").attr('target', 'fixAutofillFrame');
                $("#login-form").submit();

                return false;
            };
        }
    };
}