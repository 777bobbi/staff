function MoneyInputDirective() {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element) {

            scope.$watch('quantity', function (newValue, oldValue) {
                var arr = String(newValue);
                var float = arr.split('.')[1] || '';

                if (arr.indexOf(' ') != -1) { // prevent spaces
                    scope.quantity = oldValue || '';
                }
                if (arr.trim().length == 0) {
                    return;
                }
                if (isNaN(newValue) || newValue < 0 || float.length > 2) {
                    scope.quantity = oldValue || '';
                }
            });

            element.on('focusout', function (event) {
                scope.$apply(function () {
                    var parts = element.val().split('.');
                    var decimal = parts[0] || '0';
                    var float = parts[1] || '00';

                    if (!element.val().length || parseFloat(element.val()) === 0) {
                        scope.quantity = '';
                        return;
                    }

                    decimal = parseInt(decimal) > 0 ? decimal.replace(/^0+/, "") : decimal;
                    float = (float + '00').slice(0, 2);

                    scope.quantity = decimal + '.' + float;
                });
            });

        }
    }
}
