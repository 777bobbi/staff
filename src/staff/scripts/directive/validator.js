ValidatorDirective.$inject = ['$position', '$compile', '$document', '$translate'];
function ValidatorDirective($position, $compile, $document, $translate) {

    var patterns = {
        email: {
            pattern: /^([a-z0-9_\.\-])+@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/i,
            message: $translate.translate('GENERAL_VALID_ERROR_EMAIL')
        },
        url: {
            pattern: /^(?:[a-z0-9\.\-]*):\/\/(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|\[?[A-F0-9]*:[A-F0-9:]+\]?)(?::\d+)?(?:\/?|[\/?]\S+)$/i,
            message: $translate.translate('GENERAL_VALID_ERROR_URL')
        },
        phone: {
            pattern: /^\+?[0-9\-)(\s]{7,18}$/,   //At least 7 symbols; format: +3 (800) 322 44 55
            message: $translate.translate('GENERAL_VALID_ERROR_PHONE')
        },
        notEmpty: {
            pattern: /^.+$/,
            message: $translate.translate('GENERAL_VALID_ERROR_REQUIRED')
        },
        skype: {
            pattern: /^[a-z][a-z0-9\.,\-_]{5,31}$/i,
            message: $translate.translate('GENERAL_VALID_ERROR_SKYPE')
        },
        icq: {
            pattern: /^([0-9]\-?){7,8}[0-9]$/,
            message: $translate.translate('GENERAL_VALID_ERROR_ICQ')
        },
        alphaNumNoSpaces: {
            pattern: /^[a-z0-9\-_]{2,}$/i,
            message: $translate.translate('GENERAL_VALID_ERROR_ALPHA_NUM_NO_SPACES')
        },
        alphaNum: {
            pattern: /^[a-z0-9\-_\s]{2,}$/i,
            message: $translate.translate('GENERAL_VALID_ERROR_ALPHA_NUM')
        },
        alphaNumCyrillic: {
            pattern: /^[ёіїєа-яa-z0-9\-_\s]{2,}$/i,
            message: $translate.translate('GENERAL_VALID_ALPHA_NUM_CYRILLIC')
        },
        alphaNumCyrillicSymbols: {
            pattern: /^[ёіїєа-яa-z0-9\-_\.,\s]{2,}$/i,
            message: $translate.translate('GENERAL_VALID_ALPHA_NUM_CYRILLIC_SYMBOLS')
        },
        alphaCyrillicHyphen: {
            pattern: /^[a-zёіїєа-я]+[\-]?[a-zёіїєа-я]+$/i,  // for first_name & last_name
            message: $translate.translate('GENERAL_VALID_ALPHA_NUM_HYPHEN')
        },
        gtZero: /^[1-9][0-9]*$/,
        hasNoSpaces: /^[^\s]+$/,
        password: /^[\w\d\-+=_.]{6,15}$/,
        address: /^.{4,500}$/m,
        captcha: /^\d{4}$/,
        numbers: /^[0-9]+$/,
        message: /^.{3,800}$/m,
        day: /^[3|0|1]|[0|1|2][0-9]$/,
        year: /^[1-2][0-9]{3}$/,
        month: /^[1-9]|[1][0|1|2]{2}$/,
        mysqlDate: /^\d{4}-\d{2}-\d{2}(.{14})?$/,
        integer: /^\-?\d*$/
    };

    var notExecuteValidator = notExecuteValidator;

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            if (!ctrl) return;

            var validators = attrs.tgValidator.split(',');
            scope.errors = [];

            if (attrs.required && validators.indexOf('notEmpty') == -1) {
                validators.push('notEmpty');
            }

            var tooltipPlacement = attrs.tgValidatorTooltipPlacement || 'right'

            scope.tooltipShow = attrs.tgValidatorTooltipShow == 'true' || false;

            attrs.$observe('tgValidatorTooltipShow', function (value) {
                scope.tooltipShow = value == 'true' || false;

                if (!scope.tooltipShow) {
                    removeTooltip(scope);
                }
            })

            angular.forEach(validators, function (validatorName) {

                if (validatorName in patterns) {

                    ctrl.$formatters.push(function (modelValue) {
                        //todo: create separate directive for tooltip, create watchers for attribute 'tooltipShow' and handling tooltip by changing its state.
                        if (scope.tooltipShow) {
                            removeTooltip(scope);
                            updateErrorList(scope, validatorName, modelValue);
                            showTooltip(scope, element, tooltipPlacement);
                        }

                        if (notExecuteValidator(modelValue, validatorName)) {
                            ctrl.$setValidity(validatorName, true);
                            return modelValue;
                        }

                        ctrl.$setValidity(validatorName, patterns[validatorName].pattern.test(modelValue));
                        return modelValue;
                    });
                    ctrl.$parsers.push(function (viewValue) {
                        if (scope.tooltipShow) {
                            removeTooltip(scope);
                            updateErrorList(scope, validatorName, viewValue);
                            showTooltip(scope, element, tooltipPlacement);
                        }

                        if (notExecuteValidator(viewValue, validatorName)) {
                            ctrl.$setValidity(validatorName, true);
                            return viewValue;
                        }

                        ctrl.$setValidity(validatorName, patterns[validatorName].pattern.test(viewValue));
                        return viewValue;
                    });

                }
            })

        }
    };

    function updateErrorList(scope, validatorName, value) {
        var error;

        if (!patterns[validatorName].pattern.test(value == undefined ? '' : value) && !notExecuteValidator(value, validatorName)) {
            error = patterns[validatorName].message;
        }

        if (error && scope.errors.indexOf(error) == -1) {
            scope.errors.push(error);
        }
        if (!error && scope.errors.indexOf(patterns[validatorName].message) != -1) {
            scope.errors.splice(scope.errors.indexOf(patterns[validatorName].message), 1);
        }
    }

    function notExecuteValidator(data, patternsName) {
        return (!data || !data.length) && patternsName != 'notEmpty';
    }

    function removeTooltip(scope) {
        if (scope.tooltip) {
            scope.tooltip.remove();
        }
    }

    function showTooltip(scope, element, placement) {
        if (!scope.errors.length) {
            return;
        }

        element.addClass('tooltip-error');

        var template =
            '<div class="tooltip ' + placement + ' in">' +
                '<div class="tooltip-arrow"></div>' +
                '<div class="tooltip-inner">' +
                (scope.errors[0] || '') +
                '</div>' +
                '</div>';

        scope.tooltip = $compile(template)(scope);

        show();

        // Show the tooltip popup element.
        function show() {
            var position,
                ttWidth,
                ttHeight,
                ttPosition;

            // Set the initial positioning.
            scope.tooltip.css({ top: 0, left: 0, display: 'block' });


            //$body = $document.find( 'body' );
            //$body.append( scope.tooltip );

            element.after(scope.tooltip);

            // Get the position of the directive element.
//                position = $position.offset( element );
            position = $position.position(element);

            // Get the height and width of the tooltip so we can center it.
            ttWidth = scope.tooltip.prop('offsetWidth');
            ttHeight = scope.tooltip.prop('offsetHeight');

            // Calculate the tooltip's top and left coordinates to center it with
            // this directive.
            switch (placement) {
                case 'right':
                    ttPosition = {
                        top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
                        left: (position.left + position.width) + 'px',
                        height: ttHeight + 'px',
                        width: ttWidth + 'px'
                    };
                    break;
                case 'bottom':
                    ttPosition = {
                        top: (position.top + position.height) + 'px',
                        left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
                    };
                    break;
                case 'left':
                    ttPosition = {
                        top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
                        left: (position.left - ttWidth) + 'px'
                    };
                    break;
                default:
                    ttPosition = {
                        top: (position.top - ttHeight) + 'px',
                        left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
                    };
                    break;
            }

            // Now set the calculated positioning.
            scope.tooltip.css(ttPosition);
        }

    }

}