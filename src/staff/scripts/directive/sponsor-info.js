SponsorInfoDirective.$inject = ['CurrentUserData','$translate'];
function SponsorInfoDirective(CurrentUserData, $translate) {

    return {
        restrict: 'A',
        replace: true,
        templateUrl: '/bundles/partner/partials/block/sponsor-info.html',
        link: function (scope, element, attrs) {

            scope.isLoading = true;
            scope.sponsor = {};

            CurrentUserData.getSponsor(function responseSuccessHandler(env) {
                if (statusCode['SPONSOR_NOT_FOUND'] == env.code) {
                    element.hide();
                } else {
                    scope.sponsor = env.data;
                }

                scope.isLoading = false;
            });

            scope.openBlock = function () {
                scope.blockOpened = !scope.blockOpened;
            }

            scope.blockDisplay = true;

            scope.showBlock = function (notification) {
                if (scope.blockDisplay) {
                    scope.blockOpened = true;
                    notification.success($translate.translate('LEADERS_CONNECT_YOUR_RECOMMENDER_NOT'));
                    scope.blockDisplay = false;
                }
            }
        }
    };

}