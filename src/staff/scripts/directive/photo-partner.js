PhotoPartnerDirective.$inject = ['$notification', '$timeout', 'TigerResource', 'ProfileService', '$rootScope', '$modal', '$translate'];
function PhotoPartnerDirective($notification, $timeout, TigerResource, ProfileService, $rootScope, $modal, $translate) {

    var BASE_URL = /app_dev/.test(location.pathname) ? '/app_dev.php/api' : '/api';
    var MAX_FILE_SIZE = 16 * 1000 * 1000;
    var DUMMY_IMAGE_PATH = '/bundles/partner/images/';

    return {
        restrict: 'A',
        scope: {
            'user': '='
        },
        replace: true,
        templateUrl: '/bundles/partner/partials/block/photo-partner.html',
        link: function (scope, element, attrs) {

//            scope.user = scope[attrs.user];

            scope.imageCoords = {};
            scope.showUploadDialog = false;

            scope.loadedInPercent = 0;

            scope.imageUploaded = true;
            scope.previewUploaded = false;

            var options = attrs.options ? angular.fromJson(attrs.options.replace(/'/g, '"')) : {};
            var sizePrefix = getSizePrefix(options.imageSizeType);
            scope.editable = options.editable || false;
            scope.showNameBlock = options.showNameBlock || false;
            scope.showSocialsBlock = options.showSocialsBlock || false;
            scope.protocols = ['http://', 'https://'];
            scope.formData = false;
            var progressInterval = null;

            function startProgressbar() {
                progressInterval = setInterval(function () {
                    if (99 <= scope.loadedInPercent) {
                        return;
                    }
                    scope.loadedInPercent++;
                    scope.$apply();
                }, 50);
            }

            function stopProgressbar() {
                clearInterval(progressInterval);
                scope.loadedInPercent = 0;
            }

            function correctLink(link) {
                if (link) {
                    link = link.trim();
                    var res = '';

                    angular.forEach(scope.protocols, function (v) {
                        if (0 == link.indexOf(v)) {
                            res = link;
                        }
                    });

                    return res ? res : scope.protocols[0] + link;
                }

                return link;
            }

            scope.$watch('user', function () {
                if (scope.showSocialsBlock && scope.user && 0 < scope.user.id) {
                    ProfileService.loadSchema(function (schema) {
                        scope.schema = schema;

                        ProfileService.obtainUserData({'id': scope.user.id}, function (data) {
                            angular.forEach(['facebook', 'vkontakte', 'twitter', 'instagram'], function (v) {
                                scope.user[v] = correctLink(data[v]);
                            });
                        });
                    });
                }
            });

            if (scope.editable) {
                $timeout(function () { // because ng-if added for editable state

                    var container = $('.file-label');

                    $('#fileupload').fileupload({
                        url: BASE_URL + '/me/photo/upload',
                        maxFileSize: MAX_FILE_SIZE,
                        replaceFileInput: false,
                        dataType: 'json',
                        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                        add: function (e, data) {
                            fileuploadadd.apply(this, arguments);
                        },
                        done: function (e, data) {
                            fileuploadcompleted.apply(this, arguments);
                        }
                    });

                    function fileuploadadd(e, data) {
                        var file = data.files[0];
                        if (file.size > MAX_FILE_SIZE) {
                            //$notification.error('Максимальный размер файла 2Mb');
                            alert($translate.translate('PROFILE_AVATAR_EDIT_FILE_MAX_SIZE_16M'));
                            return;
                        }
                        scope.formData = data;
                        data.formData = {'preview': 1};
                        scope.modal = $modal.open({templateUrl: '/bundles/partner/partials/block/profile-image-crop.html', scope: scope});
                        var valid = true;
                        var request = data.submit();
                        request.error(function () {
                            valid = false;
                        });
                        data.formData = {'preview': 0};
                        if (valid) {
                            startProgressbar();
                        } else {
                            scope.modal.close()
                            scope.modal = '';
                            $notification.error($translate.translate('PROFILE_AVATAR_EDIT_WRONG_FILE_FORMAT'));
                        }
                    }

                    scope.loadPhoto = function () {
                        var data = scope.formData;
                        data.formData.x1 = scope.imageCoords.x1;
                        data.formData.x2 = scope.imageCoords.x2;
                        data.formData.y1 = scope.imageCoords.y1;
                        data.formData.y2 = scope.imageCoords.y2;
                        data.submit();
                    }

                    scope.applyCropedPhoto = function () {
                        scope.modal.close();
                        container.find('img').remove();
                        scope.imageUploaded = false;
                        scope.loadPhoto();
                        cleanPreviewData();

                    }

                    scope.cancelCropedPhoto = function () {
                        scope.imageUploaded = true;
                        scope.modal.close();
                        scope.modal = null;
                        cleanPreviewData()
                        stopProgressbar();
                    }

                    function cleanPreviewData() {
                        scope.imageWidth = null;
                        scope.imageHeight = null;

                        scope.previewUrl = null;
                        scope.previewName = null;
                        scope.previewUploaded = false;
                    }

                    function getImageSize(imageWidth, imageHeight, areaWidth, areaHeight) {
                        if (imageWidth < areaWidth && imageHeight < areaHeight) {

                            return {'width': imageWidth, 'height': imageHeight};
                        }

                        var coefficient = areaWidth / imageWidth;

                        if (imageHeight * coefficient > areaHeight || 1 <= coefficient) {

                            var coefficient = areaHeight / imageHeight;

                        }

                        return {'width': imageWidth * coefficient, 'height': imageHeight * coefficient};

                    }

                    function fileuploadcompleted(e, data) {
                        if (data.result.data.preview) {
                            if (!scope.modal) {
                                return;
                            }
                            var image = new Image();
                            image.src = data.result.data.src;
                            image.onload = function () {
                                stopProgressbar();
                                var gabarits = getImageSize(image.width, image.height, 600, 667);

                                scope.imageWidth = gabarits.width;
                                scope.imageHeight = gabarits.height;

                                scope.previewUrl = data.result.data.src;
                                scope.previewName = data.result.data.name;
                                scope.previewUploaded = true;
                                scope.$apply();
                                //scope.modal = $modal.open({templateUrl: '/bundles/partner/partials/block/profile-image-crop.html', scope: scope});
                            }
                        }
                        else {
                            scope.$apply(function () {

                                if (1000 == data.result.code) {
                                    scope.user.photo = data.result.data.src;
                                    $("img.profile-photo:not(ng-hide)").prop('src', scope.getPhoto());
                                    $notification.success($translate.translate('PROFILE_AVATAR_EDIT_UPDATED'));
                                }

                                else {
                                    $notification.error($translate.translate('PROFILE_AVATAR_EDIT_CANT_SAVE_IMAGE.'));
                                }

                                scope.imageUploaded = true;
                            })
                        }
                    }


                    scope.save = function () {
                        $('.uploadImgBtn').click();
                    }

                    scope.cancel = function () {
                        container.removeClass('hide-placeholder selected').children().removeClass('large').attr('data-title', 'No File ...').find('img').remove();
                        scope.showUploadDialog = false;
                    }

                    scope.change = function () {
                        $('[name="uploadedPhoto"]').click();
                    }

                }, 1);
            }

            scope.getPhoto = function () {
                if (scope.user && scope.user.photo) {
                    return '/uploads/' + $rootScope.system.subdomain_name + '/photo/' + sizePrefix + scope.user.photo;
                }

                return DUMMY_IMAGE_PATH + 'avatar_' + options.imageSizeType + '.jpg';
            }

            function getSizePrefix(sizeType) {
                if (!sizeType || sizeType == 'big') {
                    return 'b/';
                }
                if (sizeType == 'normal') {
                    return 'n/';
                }
                if (sizeType == 'small') {
                    return 's/';
                }

                return '';
            }

        }
    };

}
