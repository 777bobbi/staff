AntiPlagiarismDirective.$inject = ['$state'];
function AntiPlagiarismDirective($state) {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$on('$viewContentLoaded',
                function (event) {
                    var isDevMode = /app_dev/.test(location.pathname);
                    var isNotLockedPage = ['app.page', 'app.page_by_alias'].indexOf($state.current.name) == -1;

                    if (isDevMode || isNotLockedPage) {
                        unLockContent();
                        return;
                    }

                    lockContent();
                });

            function lockContent() {
                element.on('dragstart', function (e) {
                    e.preventDefault();
                    return false;
                });
                element.on('contextmenu', function (e) {
                    e.preventDefault();
                    return false;
                });

                element.css({
                    "-webkit-user-select": "none",
                    "-khtml-user-select": "none",
                    "-moz-user-select": "none",
                    "-o-user-select": "none",
                    "user-select": "none"
                });
            }

            function unLockContent() {
                element.off('dragstart');
                element.off('contextmenu');

                element.css({
                    "-webkit-user-select": "auto",
                    "-khtml-user-select": "auto",
                    "-moz-user-select": "auto",
                    "-o-user-select": "auto",
                    "user-select": "auto"
                });
            }


        }
    };

}