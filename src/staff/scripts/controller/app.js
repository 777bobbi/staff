AppController.$inject = ['$scope'];
function AppController($scope) {

    $scope.$on('$stateChangeSuccess', function (event, toState) {
        if (angular.isDefined(toState.data)) {
            $scope.breadcrumbs = toState.data.breadcrumbs;
        }
    });

    $scope.scrollToTop = function () {
        var scroll = setInterval(function () {
            window.scrollBy(0, -50);

            if (window.pageYOffset == 0) {
                clearInterval(scroll);
                $scope.scrollTopFlag = false;
            }
        }, 10);
    }

    $scope.scrollTopFlag = false;

    Hamster(window).wheel(function (event, delta, deltaX, deltaY) {
        var scrollHeight = window.pageYOffset;

        if (scrollHeight < 100) {
            console.log("less than 100");
            $scope.scrollTopFlag = false;
        }
        if (scrollHeight > 100) {
            console.log(">100");
            $scope.scrollTopFlag = true;
        }
    });


}