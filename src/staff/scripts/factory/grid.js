GridCfgBuilder.$inject = [];
function GridCfgBuilder() {

    /**
     * Generates unique identifier.
     *
     * @returns {String}
     */
    function getUniqueId() {
        return (new Date).getTime().toString();
    }

    window.ngGrid.i18n['en'] = {
        ngAggregateLabel: window.translate.translate('ngGrid.ngAggregateLabel'),
        ngGroupPanelDescription: window.translate.translate('ngGrid.ngGroupPanelDescription'),
        ngSearchPlaceHolder: window.translate.translate('ngGrid.ngSearchPlaceHolder'),
        ngMenuText: window.translate.translate('ngGrid.ngMenuText'),
        ngShowingItemsLabel: window.translate.translate('ngGrid.ngShowingItemsLabel'),
        ngTotalItemsLabel: window.translate.translate('ngGrid.ngTotalItemsLabel'),
        ngSelectedItemsLabel: window.translate.translate('ngGrid.ngSelectedItemsLabel'),
        ngPageSizeLabel: window.translate.translate('ngGrid.ngPageSizeLabel'),
        ngPagerFirstTitle: window.translate.translate('ngGrid.ngPagerFirstTitle'),
        ngPagerNextTitle: window.translate.translate('ngGrid.ngPagerNextTitle'),
        ngPagerPrevTitle: window.translate.translate('ngGrid.ngPagerPrevTitle'),
        ngPagerLastTitle: window.translate.translate('ngGrid.ngPagerLastTitle')
    };

    /**
     * @see http://angular-ui.github.io/ng-grid/#/api
     */
    return function GridCfgBuilderCallback(scope, params) {
        //console.log(params);
        /**
         * Unique identifier.
         */
        var uid = getUniqueId(),
            uid_1 = 'd_' + uid,
            uid_2 = 'n_' + uid;

        /**
         * Data being displayed in the grid. Each item in the array is mapped to a row being displayed.
         */
        params.data = uid_1;

        params.plugins = [new ngGridFlexibleHeightPlugin({minHeight: 394})];

        /**
         * Enable or disable resizing of columns.
         */
        params.enableColumnResize = true;

        /**
         * Set this to false if you only want one item selected at a time.
         */
        params.multiSelect = false;

        /**
         * Prevents the internal sorting from executing. The sortInfo object will be updated with the sorting information
         * so you can handle sorting (see sortInfo)
         */
//        params.useExternalSorting = true;

        /**
         * Define a sortInfo object to specify a default sorting state. You can also observe this variable to utilize
         * server-side sorting (see useExternalSorting). Syntax is sortInfo: { fields: ['fieldName1',' fieldName2'],
         * directions: ['asc', 'desc']}. Directions are case-insensitive
         */
//        params.sortInfo = {
//            fields: ['first_name'],
//            directions: ['desc']
//        };

        /**
         * Show or hide the footer the footer is disabled by default.
         */
        params.showFooter = true;

        /**
         * Enables the server-side paging feature.
         */
        params.enablePaging = true;

        /**
         * Total items are on the server.
         */
        params.totalServerItems = uid_2;

        /**
         * All of the items selected in the grid. In single select mode there will only be one item in the array.
         */
        params.selectedItems = [];

        /**
         * pageSizes: list of available page sizes.
         * pageSize: currently selected page size.
         * totalServerItems: Total items are on the server.
         * currentPage: the uhm... current page.
         */
        params.pagingOptions = {
            pageSizes: [10, 25, 50],
            pageSize: 10,
            totalServerItems: 0,
            currentPage: 1
        };

        /**
         * filterText: The text bound to the built-in search box.
         * useExternalFilter: Bypass internal filtering if you want to roll your own filtering mechanism but want to use builtin search box.
         */
        params.filterOptions = {
            filterText: '',
            useExternalFilter: true
        };

        params.loading = false;

        /**
         * Fetches data from data provider.
         */
        params.fetchData = function () {
            if (params.selectedItems.length > 0) {
                params.selectedItems.length = 0;
            }

            var queryParams = {
                page: params.pagingOptions.currentPage,
                pageSize: params.pagingOptions.pageSize
            };

            if (angular.isObject(params.sortInfo)) {
                queryParams.sortBy = params.sortInfo.fields[0];
                queryParams.sortDirection = params.sortInfo.directions[0];
            }
            if(params.ngGrid) {
                params.ngGrid.$root.addClass('tg-loader-screen');
            }

            params.isLoading = true;

            params.dataProvider(function fetchDataFn(data) {
                params.isLoading = false;
                if(params.ngGrid) {
                    params.ngGrid.$root.removeClass('tg-loader-screen');
                }
                scope[uid_1] = data.items;
                scope[uid_2] = data.total;
                if (!scope.$$phase) {
                    scope.$apply();
                }
            }, queryParams);
        };

        var previousPagingOptions = angular.copy(params.pagingOptions);
        var previousSortInfo = angular.copy(params.sortInfo);
        setInterval(
            function() {
                var updated = false;

                if (!angular.equals(params.pagingOptions, previousPagingOptions)) {
                    updated = true;
                    previousPagingOptions = angular.copy(params.pagingOptions);
                }

                if (params.useExternalSorting) {
                    var tmpObj = angular.copy(params.sortInfo);
                    delete(tmpObj.columns);
                    if (!angular.equals(previousSortInfo, tmpObj)) {
                        updated = true;
                        previousSortInfo = angular.copy(tmpObj);
                    }
                }

                if (updated) {
                    params.fetchData();
                }

            }, 100
        )

        return params;
    };

}