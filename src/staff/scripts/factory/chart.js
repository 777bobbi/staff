ChartCfgBuilder.$inject = [];
/**
 * All information about configuration of parts of charts (cols, options)
 * you can get from https://developers.google.com/chart/interactive/docs/reference
 * @returns {Function}
 * @constructor
 */
function ChartCfgBuilder() {

    function converterData(cols, data) {
        var tmpArr = [];
        for (i in data) {
            var row = {};
            row.c = [];
            var item = data[i];
            for (index in cols) {
                var colName = cols[index].id;
                var value = item[colName];
                if ('udefined' == typeof value) {
                    continue;
                }
                if ('date' == cols[index].type && !(value instanceof Date)) {
                    var date = new Date();
                    if (!isNaN(date.setTime(Date.parse(value)))) {
                        value = date;
                    }

                }
                if ('number' == cols[index].type) {
                    value *= 1;
                }
                row.c.push({v: value});
            }
            tmpArr.push(row);
        }
        return tmpArr;
    }

    return function () {
        return {

            data: {},

            options: {},

            setCssStyle: {},

            type: 'LineChart',

            formatters: {},

            setType: function (type) {
                this.type = type;
                return this;
            },

            /**
             *
             * @param config
             * @returns {*}
             */
            setCols: function (config) {
                this.data.cols = config;
                return this;
            },

            /**
             *
             * @param config
             * @returns {*}
             */
            setOptions: function (config) {
                this.options = config;
                return this;
            },

            /**
             *
             * @param data
             * @returns {*}
             */
            setStyle: function (data) {
                this.setCssStyle = data;
                return this;
            },

            /**
             *
             * @param data
             * @returns {*}
             */
            setData: function (data) {
                this.data.rows = converterData(this.data.cols, data);
                return this;
            },

            /**
             *
             * @param formatters
             * @returns {*}
             */
            setFormatters: function (formatters) {
                this.formatter = formatters;
                return this;
            }
        }

    }


}